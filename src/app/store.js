import { configureStore } from '@reduxjs/toolkit'
import counterReducer from '../features/counter/counterSlice'
import calculationReducer from '../features/calculation/calculationSlice'

export const store = configureStore({
  reducer: {
    counter: counterReducer,
    calculation: calculationReducer
  }
})
