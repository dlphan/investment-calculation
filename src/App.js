import React from 'react'
import './App.css'
import { Calculation } from './features/calculation/Calculation'

function App() {
  return (
    <div className='App'>
      <Calculation />
    </div>
  )
}

export default App
