import axios from 'axios'
import { useState } from 'react'

const endpoint = `${process.env.REACT_APP_ENDPOINT}${process.env.REACT_APP_VERSION}`

export const useAPI = () => {
  const [data, setData] = useState()
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState(false)

  const pingAPI = () => {
    setLoading(true)
    axios
      .get(endpoint + '/ping')
      .then((rs) => {
        setData(rs.data)
        setLoading(false)
      })
      .catch((err) => {
        setError(err)
        setLoading(false)
      })
  }

  const getCoinDataAPI = (top = 10) => {
    setLoading(true)
    axios
      .get(
        endpoint +
          '/coins/markets?vs_currency=usd&order=market_cap_desc&per_page=50&page=1&sparkline=false'
      )
      .then((rs) => {
        const stableCoinFilter = rs.data.filter(
          (item) =>
            !['tether', 'usd-coin', 'terrausd', 'binance-usd'].includes(item.id)
        )
        setData(stableCoinFilter.slice(0, top))
        setLoading(false)
      })
      .catch((err) => {
        setError(err)
        setLoading(false)
      })
  }

  return {
    data,
    loading,
    error,
    pingAPI,
    getCoinDataAPI
  }
}
