import { Form, Input, Button, InputNumber } from 'antd'
import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { LOCAL_STORAGE_ITEM } from '../../constants/common'
import {
  selectCoinsData,
  selectPortfolio,
  setPortfolio,
  setPortfolioTotal
} from './calculationSlice'
const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 }
}

export const InitForm = (props) => {
  const coinsData = useSelector(selectCoinsData)
  const portfolio = useSelector(selectPortfolio)
  const dispatch = useDispatch()

  const [form] = Form.useForm()

  const onFinish = (values) => {
    console.log(values)
    let total = 0
    for (const item in values) {
      total +=
        parseFloat(values[item]) *
        coinsData.find((coin) => coin.symbol === item).current_price
    }
    dispatch(setPortfolio({ ...values, portfolioTotal: total }))
    dispatch(setPortfolioTotal(total))
    localStorage.setItem(LOCAL_STORAGE_ITEM.PORTFOLIO, JSON.stringify(values))
    localStorage.setItem(LOCAL_STORAGE_ITEM.PORTFOLIO_TOTAL, total)

    props.setVisible(false)
  }
  const onReset = () => {
    form.resetFields()
  }

  const InputRender = coinsData.map((item) => {
    return (
      <Form.Item
        key={item.id}
        name={item.symbol}
        label={item.symbol.toUpperCase()}
        initialValue={portfolio[item.symbol] || 0}
      >
        <InputNumber min={0} style={{ width: '100%' }} />
      </Form.Item>
    )
  })

  return (
    <Form
      {...layout}
      form={form}
      name='portfolio-form'
      onFinish={onFinish}
      labelAlign='left'
    >
      {InputRender}
    </Form>
  )
}
