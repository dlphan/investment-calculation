import { ExclamationCircleOutlined } from '@ant-design/icons'
import { Button, Drawer, Input, InputNumber, Modal, Table } from 'antd'
import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { LOCAL_STORAGE_ITEM } from '../../constants/common'
import { useAPI } from '../../hooks/useAPI'
import {
  selectCoinsData,
  selectPortfolio,
  selectPortfolioTotal,
  selectTotalMarkerCap,
  setCoinsData,
  setPortfolio,
  setPortfolioTotal
} from './calculationSlice'
import { InitForm } from './InitForm'

const { confirm } = Modal
export const Calculation = () => {
  const { data, loading, error, getCoinDataAPI } = useAPI()
  const dispatch = useDispatch()
  const coinsData = useSelector(selectCoinsData)
  const totalMarketCap = useSelector(selectTotalMarkerCap)
  const portfolio = useSelector(selectPortfolio)
  const portfolioTotal = useSelector(selectPortfolioTotal)
  const [money, setMoney] = useState(0)
  const [visible, setVisible] = useState(false)

  useEffect(() => {
    getCoinDataAPI(10)
  }, [])

  useEffect(() => {
    if (data?.length > 0 && !loading) {
      dispatch(setCoinsData(data))
    }
  }, [data, loading])

  const columns = [
    {
      title: 'Index',
      dataIndex: 'index',
      key: 'index',
      width: 70,
      align: 'right'
    },
    {
      title: 'Coin',
      dataIndex: 'symbol',
      key: 'symbol',
      render: (value) => {
        return value.toUpperCase()
      },
      align: 'right'
    },
    {
      title: 'Portfolio ($)',
      dataIndex: 'portfolio',
      key: 'portfolio',
      align: 'right',
      render: (portfolio, record) => {
        return (portfolio * record.current_price).toLocaleString()
        // return (portfolio[symbol] * record.current_price).toLocaleString()
      }
    },
    {
      title: 'Portfolio (%)',
      dataIndex: 'portfolio',
      key: 'portfolio',
      align: 'right',
      render: (portfolio, record) => {
        return `${
          portfolio
            ? (
                ((portfolio * record.current_price) / portfolioTotal) *
                100
              ).toFixed(2)
            : 0
        }%`
      }
    },
    {
      title: 'Market cap ($)',
      dataIndex: 'market_cap',
      key: 'market_cap',
      align: 'right',
      render: (marketCap) => marketCap.toLocaleString()
    },
    {
      title: 'Market cap (%)',
      dataIndex: 'market_cap',
      key: 'market_cap',
      render: (marketCap) => {
        return `${((marketCap / totalMarketCap) * 100).toFixed(2)}%`
      },
      align: 'right'
    },
    {
      title: 'Delta',
      dataIndex: 'delta',
      key: 'delta',
      align: 'right',
      render: (delta, record) => {
        // const delta = (
        //   (marketCap / totalMarketCap -
        //     (record.portfolio * record.current_price) / portfolioTotal) *
        //   100
        // ).toFixed(2)

        return (
          <div style={{ color: delta >= 0 ? 'green' : 'red' }}>{`${
            !isNaN(delta) ? delta : 0
          }%`}</div>
        )
      }
    },
    {
      title: 'Buy',
      width: 150,
      dataIndex: 'delta',
      key: 'delta',
      align: 'right',
      render: (delta) => {
        console.log(money)
        const coinsBuyMore = coinsData.filter((item) => item.delta > 0)
        return delta > 0 ? (money / coinsBuyMore.length).toFixed(2) : 0
      }
    }
  ]

  return (
    <div style={{ padding: '1rem' }}>
      <div
        style={{
          marginBottom: '1rem',
          display: 'flex',
          justifyContent: 'space-between'
        }}
      >
        <div>
          <InputNumber
            style={{ marginRight: '1rem', width: 200 }}
            placeholder='Input money'
            addonAfter='$'
            // value={money}
            onChange={(value) => {
              setMoney(value)
            }}
          />
          <Button
            type='primary'
            onClick={() => {
              confirm({
                title: 'Confirm',
                icon: <ExclamationCircleOutlined />,
                content: 'Do you Want to buy?',
                onOk() {
                  const coinsBuyMore = coinsData.filter(
                    (item) => item.delta > 0
                  )
                  const moneyToBuy = (money / coinsBuyMore.length).toFixed(2)
                  const newPortfolio = { ...portfolio }
                  // const coinsToBuy = coinsBuyMore.map((item) => item.symbol)

                  coinsBuyMore.forEach((item) => {
                    newPortfolio[item.symbol] =
                      parseFloat(newPortfolio[item.symbol]) +
                      moneyToBuy / item.current_price
                  })

                  console.log(newPortfolio)

                  let total = 0
                  for (const item in newPortfolio) {
                    if (item !== 'portfolioTotal') {
                      total +=
                        parseFloat(newPortfolio[item]) *
                        coinsData.find((coin) => coin.symbol === item)
                          .current_price
                    }

                    // debugger
                  }
                  dispatch(
                    setPortfolio({ ...newPortfolio, portfolioTotal: total })
                  )
                  dispatch(setPortfolioTotal(total))
                  localStorage.setItem(
                    LOCAL_STORAGE_ITEM.PORTFOLIO,
                    JSON.stringify(newPortfolio)
                  )
                  localStorage.setItem(
                    LOCAL_STORAGE_ITEM.PORTFOLIO_TOTAL,
                    total
                  )
                },
                onCancel() {
                  console.log('Cancel')
                }
              })
            }}
          >
            Buy
          </Button>
        </div>
        <Button
          type='primary'
          onClick={() => {
            setVisible(true)
          }}
        >
          Edit Portfolio
        </Button>
      </div>
      <Table
        size='middle'
        bordered
        dataSource={coinsData}
        columns={columns}
        pagination={{
          hideOnSinglePage: true
        }}
        summary={(pageData) => {
          console.log(pageData)
          return (
            <>
              <Table.Summary.Row style={{ fontWeight: 700 }}>
                <Table.Summary.Cell align='right'>Total</Table.Summary.Cell>
                <Table.Summary.Cell align='right'></Table.Summary.Cell>
                <Table.Summary.Cell align='right'>
                  {portfolioTotal.toLocaleString()}
                </Table.Summary.Cell>
                <Table.Summary.Cell align='right'>{`100%`}</Table.Summary.Cell>
                <Table.Summary.Cell align='right'>
                  {totalMarketCap.toLocaleString()}
                </Table.Summary.Cell>
                <Table.Summary.Cell align='right'>{`100%`}</Table.Summary.Cell>
                <Table.Summary.Cell align='right'></Table.Summary.Cell>
                <Table.Summary.Cell align='right'>{money}</Table.Summary.Cell>
              </Table.Summary.Row>
            </>
          )
        }}
      />
      <Drawer
        title='Portfolio'
        visible={visible}
        maskClosable
        onClose={() => {
          setVisible(false)
        }}
        footer={
          <div style={{ display: 'flex', justifyContent: 'space-between' }}>
            <Button
              style={{ width: '48%' }}
              onClick={() => {
                setVisible(false)
              }}
            >
              Cancel
            </Button>
            <Button
              style={{ width: '48%' }}
              htmlType='submit'
              type='primary'
              form='portfolio-form'
            >
              Submit
            </Button>
          </div>
        }
      >
        <InitForm setVisible={setVisible} />
      </Drawer>
    </div>
  )
}
