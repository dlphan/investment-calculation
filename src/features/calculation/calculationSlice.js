import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { LOCAL_STORAGE_ITEM } from '../../constants/common'

const initialState = {
  coinsData: [],
  totalMarketCap: 0,
  portfolio:
    JSON.parse(localStorage.getItem(LOCAL_STORAGE_ITEM.PORTFOLIO)) || {},
  portFolioTotal: 0
}

export const calculationSlice = createSlice({
  name: 'calculation',
  initialState,
  // The `reducers` field lets us define reducers and generate associated actions
  reducers: {
    setCoinsData: (state, action) => {
      console.log(action.payload)
      const totalMarket = action.payload.reduce((a, b) => {
        return a + b.market_cap
      }, 0)
      const portfolioTotal = action.payload.reduce((a, b) => {
        return a + b.current_price * (state.portfolio[b.symbol] || 0)
      }, 0)

      state.coinsData = action.payload.map((item, index) => {
        const delta = (
          (item.market_cap / totalMarket -
            ((state.portfolio[item.symbol] || 0) * item.current_price) /
              portfolioTotal) *
          100
        ).toFixed(2)

        return {
          ...item,
          index: index + 1,
          key: item.id,
          portfolio: state.portfolio[item.symbol] || 0,
          delta: delta || 0
        }
      })
      state.totalMarketCap = totalMarket
      state.portFolioTotal = portfolioTotal
    },
    setPortfolio: (state, action) => {
      state.portfolio = action.payload
      state.coinsData = state.coinsData.map((item) => {
        const delta = (
          (item.market_cap / state.totalMarketCap -
            (state.portfolio[item.symbol] * item.current_price) /
              action.payload.portfolioTotal) *
          100
        ).toFixed(2)
        return {
          ...item,
          portfolio: parseFloat(action.payload[item.symbol]),
          delta: parseFloat(delta)
        }
      })
    },
    setPortfolioTotal: (state, action) => {
      state.portFolioTotal = action.payload
    }
  }
})

export const { setCoinsData, setPortfolio, setPortfolioTotal } =
  calculationSlice.actions

export const selectCoinsData = (state) => state.calculation.coinsData
export const selectTotalMarkerCap = (state) => state.calculation.totalMarketCap
export const selectPortfolio = (state) => state.calculation.portfolio
export const selectPortfolioTotal = (state) => state.calculation.portFolioTotal

export default calculationSlice.reducer
